/**
 * @author Kubyshkin Roman
 */
package com.getjavajob.training.algo.util;

import java.util.List;
import java.util.Arrays;

public class Assert {
    public static void assertEquals(String test, int expected, int actual) {
        if (expected == actual) {
            System.out.println(test + " passed");
        } else {
            System.out.println(test + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String test, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(test + " passed");
        } else {
            System.out.println(test + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String test, double expected, double actual) {
        if (expected == actual) {
            System.out.println(test + " passed");
        } else {
            System.out.println(test + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String test, String expected, String actual) {
        if (expected.compareTo(actual) == 0) {
            System.out.println(test + " passed");
        } else {
            System.out.println(test + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String test, double[] expected, double[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(test + " passed");
        } else {
            System.out.println(test + " failed: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String test, int[] expected, int[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(test + " passed");
        } else {
            System.out.println(test + " failed: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String test, StringBuilder expected, StringBuilder actual) {
        if (expected.toString().equals(actual.toString())) {
            System.out.println(test + " passed");
        } else {
            System.out.println(test + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String test, char[] expected, char[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(test + " passed");
        } else {
            System.out.println(test + " failed: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String test, int[][] expected, int[][] actual) {
        for (int i = 0; i < actual.length; i++) {
            if (Arrays.equals(expected[i], actual[i])) {
                if (i == actual.length - 1) {
                    System.out.println(test + " passed");
                }
            } else {
                System.out.println(test + " failed: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
            }
        }
    }

    public static void assertEquals(String test, String[][] expected, String[][] actual) {
        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[i].length; j++) {
                if (expected[i][j].equals(actual[i][j])) {
                    if (i == actual.length - 1 && j == actual[0].length - 1) {
                        System.out.println(test + " passed");
                    }
                } else {
                    System.out.println(test + " failed: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
                }
            }
        }
    }

    public static void assertEquals(String test, List expected, List actual) {
        for (int i = 0; i < expected.size(); i++) {
            if (actual.equals(expected)) {
                System.out.println(test + " passed");
            } else {
                System.out.println(test + " failed: expected " + Arrays.toString(expected.toArray()) + ", actual " + Arrays.toString(actual.toArray()));
            }
        }
    }
}
