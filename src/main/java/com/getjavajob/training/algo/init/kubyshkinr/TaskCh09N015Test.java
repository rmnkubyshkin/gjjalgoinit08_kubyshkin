package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh09N015.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N015Test {
    public static void main(String[] args) {
        testCharAt();
    }

    private static void testCharAt() {
        assertEquals("TaskCh09N015Test.testCharAt", 'l', charAt("Hello!", 3));
    }
}