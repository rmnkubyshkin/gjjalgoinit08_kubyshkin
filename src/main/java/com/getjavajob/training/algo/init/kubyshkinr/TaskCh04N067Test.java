package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh04N067.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

final class TaskCh04N067Test {
    public static void main(String[] args) {
        testcalcDayOfWeek();
    }

    private static void testcalcDayOfWeek() {
        assertEquals("TaskCh04N067Test.testcalcDayOfWeek workday", "Workday", calcDayOfWeek(5));
        assertEquals("TaskCh04N067Test.testcalcDayOfWeek weekend", "Weekend", calcDayOfWeek(7));
    }
}