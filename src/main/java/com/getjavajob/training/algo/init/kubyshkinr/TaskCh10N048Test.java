package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh10N048.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N048Test {
    public static void main(String[] args) {
        testFindMaxElement();
    }

    private static void testFindMaxElement() {
        int[] array = new int[]{100, 200, 4, 3, 1000};
        assertEquals("TaskCh10N048Test.testFindMaxElement", 1000, findMaxElement(4, array));
    }
}