package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.Arrays;

public class TaskCh12N025 {
    public static void main(String[] args) {
        printArray(calcArrayA());
        printArray(calcArrayB());
        printArray(calcArrayV());
        printArray(calcArrayG());
        printArray(calcArrayD());
        printArray(calcArrayE());
        printArray(calcArrayZH());
        printArray(calcArrayZ());
        printArray(calcArrayI());
        printArray(calcArrayK());
        printArray(calcArrayL());
        printArray(calcArrayM());
        printArray(calcArrayN());
        printArray(calcArrayO());
        printArray(calcArrayP());
        printArray(calcArrayR());
    }

    private static void printArray(int[][] array) {
        System.out.println(Arrays.deepToString(array));
    }

    public static int[][] calcArrayA() {
        int value = 1;
        int[][] array = new int[12][10];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                array[i][j] = value++;
            }
        }
        return array;
    }

    private static int[][] calcArrayB() {
        int value = 1;
        int[][] array = new int[12][10];
        for (int i = 0, j = 0; j < array.length; j++) {
            array[j][i] = value++;
        }
        for (int i = 1; i < array[0].length; i++) {
            for (int j = 0; j < array.length; j++) {
                array[j][i] = value++;
            }
        }
        return array;
    }

    private static int[][] calcArrayV() {
        int value = 1;
        int[][] array = new int[12][10];
        for (int i = 0; i < array.length; i++) {
            for (int j = array[0].length - 1; j >= 0; j--) {
                array[i][j] = value++;
            }
        }
        return array;
    }

    private static int[][] calcArrayG() {
        int value = 1;
        int[][] array = new int[12][10];
        for (int j = array.length - 1, i = 0; j >= 0; j--) {
            array[j][i] = value++;
        }
        for (int i = 1; i < array[0].length; i++) {
            for (int j = array.length - 1; j >= 0; j--) {
                array[j][i] = value++;
            }
        }
        return array;
    }

    private static int[][] calcArrayD() {
        int value = 1;
        int[][] array = new int[10][12];
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < array[0].length; j++) {
                    array[i][j] = value++;
                }
            } else {
                for (int j = array[0].length - 1; j >= 0; j--) {
                    array[i][j] = value++;
                }
            }
        }
        return array;
    }

    private static int[][] calcArrayE() {
        int value = 1;
        int[][] array = new int[12][10];
        for (int i = 0; i < array[0].length; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < array.length; j++) {
                    array[j][i] = value++;
                }
            } else {
                for (int j = array.length - 1; j >= 0; j--) {
                    array[j][i] = value++;
                }
            }
        }
        return array;
    }

    private static int[][] calcArrayZH() {
        int value = 1;
        int[][] array = new int[12][10];
        for (int i = array.length - 1; i >= 0; i--) {
            for (int j = 0; j < array[0].length; j++) {
                array[i][j] = value++;
            }
        }
        return array;
    }

    private static int[][] calcArrayZ() {
        int value = 1;
        int[][] array = new int[12][10];
        for (int i = array[0].length - 1; i >= 0; i--) {
            for (int j = 0; j < array.length; j++) {
                array[j][i] = value++;
            }
        }
        return array;
    }

    private static int[][] calcArrayI() {
        int value = 1;
        int[][] array = new int[12][10];
        for (int i = array.length - 1; i >= 0; i--) {
            for (int j = array[0].length - 1; j >= 0; j--) {
                array[i][j] = value++;
            }
        }
        return array;
    }

    private static int[][] calcArrayK() {
        int value = 1;
        int[][] array = new int[12][10];
        for (int i = array[0].length - 1; i >= 0; i--) {
            for (int j = array.length - 1; j >= 0; j--) {
                array[j][i] = value++;
            }
        }
        return array;
    }

    private static int[][] calcArrayL() {
        int value = 1;
        int[][] array = new int[12][10];
        for (int i = array.length - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = array[0].length - 1; j >= 0; j--) {
                    array[i][j] = value++;
                }
            } else {
                for (int j = 0; j < array[0].length; j++) {
                    array[i][j] = value++;
                }
            }
        }
        return array;
    }

    private static int[][] calcArrayM() {
        int value = 1;
        int[][] array = new int[12][10];
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                for (int j = array[0].length - 1; j >= 0; j--) {
                    array[i][j] = value++;
                }
            } else {
                for (int j = 0; j < array[0].length; j++) {
                    array[i][j] = value++;
                }
            }
        }
        return array;
    }

    private static int[][] calcArrayN() {
        int value = 1;
        int[][] array = new int[12][10];
        for (int i = array[0].length - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = array.length - 1; j >= 0; j--) {
                    array[j][i] = value++;
                }
            } else {
                for (int j = 0; j < array.length; j++) {
                    array[j][i] = value++;
                }
            }
        }
        return array;
    }

    private static int[][] calcArrayO() {
        int value = 1;
        int[][] array = new int[12][10];
        for (int i = 0; i < array[0].length; i++) {
            if (i % 2 == 0) {
                for (int j = array.length - 1; j >= 0; j--) {
                    array[j][i] = value++;
                }
            } else {
                for (int j = 0; j < array.length; j++) {
                    array[j][i] = value++;
                }
            }
        }
        return array;
    }

    private static int[][] calcArrayP() {
        int value = 1;
        int[][] array = new int[12][10];
        for (int i = array.length - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = 0; j < array[0].length; j++) {
                    array[i][j] = value++;
                }
            } else {
                for (int j = array[0].length - 1; j >= 0; j--) {
                    array[i][j] = value++;
                }
            }
        }
        return array;
    }

    private static int[][] calcArrayR() {
        int value = 1;
        int[][] array = new int[12][10];
        for (int i = array[0].length - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = 0; j < array.length; j++) {
                    array[j][i] = value++;
                }
            } else {
                for (int j = array.length - 1; j >= 0; j--) {
                    array[j][i] = value++;
                }
            }
        }
        return array;
    }
}