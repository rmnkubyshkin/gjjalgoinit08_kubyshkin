package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh12N023.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N023Test {
    public static void main(String[] args) {
        testCalcArray1();
        testCalcArray2();
        testCalcArray3();
    }

    private static void testCalcArray1() {
        int[][] expectArray = {
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1},
        };
        assertEquals("TaskCh12N023Test.testCalcArray1", expectArray, calcArray1());
    }

    private static void testCalcArray2() {
        int[][] expectArray = {
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1},
        };
        assertEquals("TaskCh12N023Test.testCalcArray2", expectArray, calcArray2());
    }

    private static void testCalcArray3() {
        int[][] expectArray = {
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1},
        };
        assertEquals("TaskCh12N023Test.testCalcArray3", expectArray, calcArray3());
    }
}