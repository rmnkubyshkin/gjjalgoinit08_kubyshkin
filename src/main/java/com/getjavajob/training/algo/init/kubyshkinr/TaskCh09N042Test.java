package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh09N042.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

class TaskCh09N042Test {
    public static void main(String[] args) {
        testInvertStr();
    }

    private static void testInvertStr() {
        assertEquals("TaskCh09N042Test.testInvertStr", "!EMOCLEW ERA UOY", invertStr("YOU ARE WELCOME!"));
    }
}