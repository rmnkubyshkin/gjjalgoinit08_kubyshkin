package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh04N033.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

final class TaskCh04N033Test {
    public static void main(String[] args) {
        testIsLastDigitEvenOrOdd();
    }

    private static void testIsLastDigitEvenOrOdd() {
        assertEquals("TaskCh04N033Test.testIsLastDigitEvenOrOdd", true, isLastDigitEvenOrOdd(4));
        assertEquals("TaskCh04N033Test.testIsLastDigitEvenOrOdd", false, isLastDigitEvenOrOdd(3));
    }
}