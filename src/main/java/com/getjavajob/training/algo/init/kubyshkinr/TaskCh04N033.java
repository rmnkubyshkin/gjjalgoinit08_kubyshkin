package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

final class TaskCh04N033 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(isLastDigitEvenOrOdd(input.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static boolean isLastDigitEvenOrOdd(int num) {
        return num % 2 == 0;
    }
}