package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh04N106.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

final class TaskCh04N106Test {
    public static void main(String[] args) {
        testGetSeason();
    }

    private static void testGetSeason() {
        assertEquals("TaskCh04N106Test.testSeasonWinter", "Winter", getSeason(12));
        assertEquals("TaskCh04N106Test.testSeasonAutumn", "Autumn", getSeason(10));
        assertEquals("TaskCh04N106Test.testSeasonSpring", "Spring", getSeason(4));
        assertEquals("TaskCh04N106Test.testSeasonSummer", "Summer", getSeason(6));
    }
}