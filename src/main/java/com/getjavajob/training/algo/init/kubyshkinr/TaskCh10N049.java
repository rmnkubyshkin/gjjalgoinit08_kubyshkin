package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

class TaskCh10N049 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int n = input.nextInt();
            int[] array = getArray(n);
            System.out.println(findIndexMaxElement(findMaxElement(array.length - 1, array), array));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    private static int[] getArray(int n) {
        Scanner input = new Scanner(System.in);
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = input.nextInt();
        }
        return array;
    }

    public static int findMaxElement(int n, int[] array) {
        if (n == 0) {
            return array[0];
        }
        int findMax = findMaxElement(n - 1, array);
        if (array[n] > findMax) {
            return array[n];
        } else {
            return findMax;
        }
    }

    public static int findIndexMaxElement(int n, int[] array) {
        int max = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == n) {
                max = i;
            }
        }
        return max;
    }
}