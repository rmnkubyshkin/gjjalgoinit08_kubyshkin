package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh12N063 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int[][] array = new int[11][4];
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array[0].length; j++) {
                    array[i][j] = input.nextInt();
                }
            }
            printArray(averageNumberStudents(array));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    private static void printArray(int[] array) {
        System.out.println(Arrays.toString(array));
    }

    public static int[] averageNumberStudents(int[][] array) {
        int[] res = new int[array.length];
        for (int i = 0; i < res.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                res[i] += array[i][j];
            }
            res[i] /= 4;
        }
        return res;
    }
}