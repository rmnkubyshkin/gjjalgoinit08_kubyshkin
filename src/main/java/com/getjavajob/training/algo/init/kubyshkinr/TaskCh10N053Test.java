package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh10N053.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N053Test {
    public static void main(String[] args) {
        testInvertNumbers();
    }

    private static void testInvertNumbers() {
        int[] expectArray = {5, 4, 3, 2, 1};
        int[] array = new int[]{1, 2, 3, 4, 5};
        assertEquals("TaskCh10N053Test.testInvertNumbers", expectArray, invertNumbers(5, array));
    }
}
