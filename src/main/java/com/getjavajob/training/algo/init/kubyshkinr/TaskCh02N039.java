package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

final class TaskCh02N039 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(calcDegree(input.nextInt(), input.nextInt(), input.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static double calcDegree(double hours, double minutes, double seconds) {
        return 360 * (hours * 3600 + minutes * 60 + seconds) / 43200;
    }
}