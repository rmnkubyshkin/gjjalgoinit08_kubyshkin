package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

class TaskCh04N015 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(calcAge(input.nextInt(), input.nextInt(), input.nextInt(), input.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static int calcAge(int monthActual, int yearActual, int monthBirth, int yearBirth) {
        return monthActual >= monthBirth ? yearActual - yearBirth : yearActual - yearBirth - 1;
    }
}