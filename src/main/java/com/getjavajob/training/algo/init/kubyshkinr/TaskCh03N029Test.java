package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh03N029.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh03N029Test {
    public static void main(String[] args) {
        testFormulaA();
        testFormulaB();
        testFormulaC();
        testFormulaD();
        testFormulaE();
        testFormulaF();
    }

    private static void testFormulaA() {
        assertEquals("TaskCh03N029Test.testFormulaA1", true, formulaA(1, 3));
        assertEquals("TaskCh03N029Test.testFormulaA2", false, formulaA(2, 4));
        assertEquals("TaskCh03N029Test.testFormulaA3", false, formulaA(2, 1));
        assertEquals("TaskCh03N029Test.testFormulaA4", false, formulaA(1, 4));
    }

    private static void testFormulaB() {
        assertEquals("TaskCh03N029Test.testFormulaB1", true, formulaB(1, 66));
        assertEquals("TaskCh03N029Test.testFormulaB2", true, formulaB(25, 4));
        assertEquals("TaskCh03N029Test.testFormulaB3", false, formulaB(55, 66));
        assertEquals("TaskCh03N029Test.testFormulaB4", false, formulaB(1, 3));
    }

    private static void testFormulaC() {
        assertEquals("TaskCh03N029Test.testFormulaC1", true, formulaC(1, 0));
        assertEquals("TaskCh03N029Test.testFormulaC2", true, formulaC(0, 1));
        assertEquals("TaskCh03N029Test.testFormulaC3", false, formulaC(1, 1));
        assertEquals("TaskCh03N029Test.testFormulaC4", true, formulaC(0, 0));
    }

    private static void testFormulaD() {
        assertEquals("TaskCh03N029Test.testFormulaD1", true, formulaD(-1, -2, -3));
        assertEquals("TaskCh03N029Test.testFormulaD2", false, formulaD(1, -2, -3));
        assertEquals("TaskCh03N029Test.testFormulaD3", false, formulaD(-1, 2, -3));
        assertEquals("TaskCh03N029Test.testFormulaD4", false, formulaD(-1, -2, 3));
        assertEquals("TaskCh03N029Test.testFormulaD5", false, formulaD(1, 2, 3));
        assertEquals("TaskCh03N029Test.testFormulaD6", false, formulaD(1, 2, -3));
        assertEquals("TaskCh03N029Test.testFormulaD7", false, formulaD(-1, 2, 3));
        assertEquals("TaskCh03N029Test.testFormulaD8", false, formulaD(1, -2, 3));
    }

    private static void testFormulaE() {
        assertEquals("TaskCh03N029Test.testFormulaE1", true, formulaE(50, 2, 3));
        assertEquals("TaskCh03N029Test.testFormulaE2", true, formulaE(1, 50, 3));
        assertEquals("TaskCh03N029Test.testFormulaE3", true, formulaE(1, 2, 50));
        assertEquals("TaskCh03N029Test.testFormulaE4", false, formulaE(1, 2, 3));
        assertEquals("TaskCh03N029Test.testFormulaE5", false, formulaE(50, 2, 50));
        assertEquals("TaskCh03N029Test.testFormulaE6", false, formulaE(50, 50, 50));
        assertEquals("TaskCh03N029Test.testFormulaE7", false, formulaE(1, 50, 50));
        assertEquals("TaskCh03N029Test.testFormulaE8", false, formulaE(50, 50, 3));
    }

    private static void testFormulaF() {
        assertEquals("TaskCh03N029Test.testFormulaF1", true, formulaF(103, 2, 3));
        assertEquals("TaskCh03N029Test.testFormulaF2", true, formulaF(1, 103, 3));
        assertEquals("TaskCh03N029Test.testFormulaF3", true, formulaF(1, 2, 103));
        assertEquals("TaskCh03N029Test.testFormulaF4", true, formulaF(103, 203, 3));
        assertEquals("TaskCh03N029Test.testFormulaF5", true, formulaF(103, 2, 303));
        assertEquals("TaskCh03N029Test.testFormulaF6", true, formulaF(1, 203, 303));
        assertEquals("TaskCh03N029Test.testFormulaF7", true, formulaF(103, 203, 303));
        assertEquals("TaskCh03N029Test.testFormulaF8", false, formulaF(1, 2, 3));
    }
}