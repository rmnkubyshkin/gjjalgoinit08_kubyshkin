package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh09N022.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

class TaskCh09N022Test {
    public static void main(String[] args) {
        testGetFirstHalfOfWord();
    }

    private static void testGetFirstHalfOfWord() {
        assertEquals("TaskCh09N022Test.testGetFirstHalfOfWord", "Hel", getFirstHalfOfWord("HelloH"));
    }
}