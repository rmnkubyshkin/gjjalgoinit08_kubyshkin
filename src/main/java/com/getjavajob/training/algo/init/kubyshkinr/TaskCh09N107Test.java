package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh09N107.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

class TaskCh09N107Test {
    public static void main(String[] args) {
        testSwapFirstALastO();
    }

    private static void testSwapFirstALastO() {
        assertEquals("TaskCh09N107Test.testSwapFirstALastO", "Moma omila romua", swapFirstALastO("Mama omila romuo"));
    }
}