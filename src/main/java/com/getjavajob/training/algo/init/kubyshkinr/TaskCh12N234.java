package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh12N234 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.println("Input array dimension: x & y");
            int[][] array = new int[input.nextInt()][input.nextInt()];
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array[0].length; j++) {
                    array[i][j] = input.nextInt();
                }
            }
            System.out.println("Input line, for delete");
            deleteLine(array, input.nextInt());
            System.out.println(Arrays.deepToString(array));
            System.out.println("Input column, for delete");
            deleteColumn(array, input.nextInt());
            System.out.println(Arrays.deepToString(array));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static void deleteLine(int[][] array, int line) {
        for (int i = line; i < array.length - 1; i++) {
            array[i] = array[i + 1];
        }
        array[array.length - 1] = new int[array[0].length];
    }

    public static void deleteColumn(int[][] array, int column) {
        for (int j = column; j < array[column].length - 1; j++) {
            for (int i = 0; i < array.length; i++) {
                array[i][j] = array[i][j + 1];
            }
        }
        for (int[] values : array) {
            values[values.length - 1] = 0;
        }
    }
}