package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh10N043.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N043Test {
    public static void main(String[] args) {
        testCalcSumOfDigits();
        testCalcNumbersOfDigits();
    }

    private static void testCalcSumOfDigits() {
        assertEquals("TaskCh10N043Test.testCalcSumOfDigits", 15, calcSumOfDigits(12345));
    }

    private static void testCalcNumbersOfDigits() {
        assertEquals("TaskCh10N043Test.testCalcNumbersOfDigits", 5, calcNumbersOfDigits(12345));
    }
}
