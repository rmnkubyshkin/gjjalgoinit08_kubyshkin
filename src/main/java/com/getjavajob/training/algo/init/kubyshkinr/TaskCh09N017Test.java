package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh09N017.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

class TaskCh09N017Test {
    public static void main(String[] args) {
        testIsBeginsAndEndsWithSameLetter();
    }

    private static void testIsBeginsAndEndsWithSameLetter() {
        assertEquals("TaskCh09N017Test.testIsBeginsAndEndsWithSameLetter", true, isBeginsAndEndsWithSameLetter("hello!h"));
    }
}