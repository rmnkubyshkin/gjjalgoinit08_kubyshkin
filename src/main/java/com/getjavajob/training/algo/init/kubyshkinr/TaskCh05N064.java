package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.InputMismatchException;
import java.util.Scanner;

class TaskCh05N064 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[][] numPersonAndArea = new int[12][2];
        try {
            for (int i = 0; i < numPersonAndArea.length; i++) {
                numPersonAndArea[i][0] = input.nextInt();
                numPersonAndArea[i][1] = input.nextInt();
            }
            System.out.print(calcPopulationDensity(numPersonAndArea));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static int calcPopulationDensity(int[][] numPersonAndArea) {
        int numerator = 0;
        int denominator = 0;
        for (int[] array : numPersonAndArea) {
            numerator += array[0];
            denominator += array[1];
        }
        return numerator / denominator;
    }
}