package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh12N063.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N063Test {
    public static void main(String[] args) {
        testAverageNumberStudents();
    }

    private static void testAverageNumberStudents() {
        int[] expectArray = {18, 18, 16, 17, 15, 19, 13, 14, 16, 17, 18};
        int[][] array = {{25, 15, 13, 21},
                {13, 23, 11, 25},
                {16, 17, 13, 21},
                {21, 26, 11, 13},
                {13, 23, 10, 14},
                {22, 23, 19, 15},
                {12, 10, 10, 22},
                {12, 23, 9, 12},
                {12, 22, 12, 20},
                {14, 21, 14, 21},
                {15, 21, 13, 25}};
        assertEquals("TaskCh12N063Test.testAverageNumberStudents", expectArray, averageNumberStudents(array));
    }
}