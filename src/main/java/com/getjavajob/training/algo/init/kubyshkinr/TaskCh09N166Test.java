package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh09N166.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

class TaskCh09N166Test {
    public static void main(String[] args) {
        testSwapFirstAndLastWord();
    }

    private static void testSwapFirstAndLastWord() {
        assertEquals("TaskCh09N166Test.testSwapFirstAndLastWord", "BBB. DEF, AAA ABC", swapFirstAndLastWord("ABC DEF, AAA BBB."));
    }
}