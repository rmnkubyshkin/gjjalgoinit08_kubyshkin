package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh11N158 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int n = input.nextInt();
            int[] array = getArray(n);
            printArray(removeSameNumbers(array));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    private static void printArray(int[] array) {
        System.out.println(Arrays.toString(array));
    }

    private static int[] getArray(int n) {
        Scanner input = new Scanner(System.in);
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = input.nextInt();
        }
        return array;
    }

    private static void calculate(int[] array, int indexJ) {
        for (int k = 0; k < array.length; k++) {
            if (indexJ == array.length - 1) {
                array[indexJ] = 0;
            } else {
                array[indexJ] = array[indexJ + 1];
                indexJ++;
            }
        }
    }

    public static int[] removeSameNumbers(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    calculate(array, j);
                }
            }
        }
        return array;
    }
}