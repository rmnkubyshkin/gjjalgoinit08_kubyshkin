package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh09N022 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(getFirstHalfOfWord(input.nextLine()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static String getFirstHalfOfWord(String string) {
        return string.substring(0, string.length() / 2);
    }
}