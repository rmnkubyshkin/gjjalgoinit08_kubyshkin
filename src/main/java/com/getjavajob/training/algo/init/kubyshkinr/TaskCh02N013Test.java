package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh02N013.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

final class TaskCh02N013Test {
    public static void main(String[] args) {
        testReverseNumb();
    }

    private static void testReverseNumb() {
        assertEquals("TaskCh02N013Test.testReverseNumb", 321, calcInvert(123));
    }
}