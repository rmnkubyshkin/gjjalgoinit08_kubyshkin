package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh05N064.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

final class TaskCh05N064Test {
    public static void main(String[] args) {
        testCalcPopulationDensity();
    }

    private static void testCalcPopulationDensity() {
        int[][] numPersonAndArea = {
                {5000, 25},
                {6000, 15},
                {6000, 56},
                {1500, 43},
                {2000, 65},
                {3000, 34},
                {5000, 12},
                {6000, 56},
                {6000, 43},
                {1500, 34},
                {2000, 12},
                {3000, 54}
        };
        assertEquals("TaskCh05N010Test.testCalcPopulationDensity", 104, calcPopulationDensity(numPersonAndArea));
    }
}