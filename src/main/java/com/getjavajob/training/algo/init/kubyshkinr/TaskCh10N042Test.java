package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh10N042.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N042Test {
    public static void main(String[] args) {
        testInvolution();
    }

    private static void testInvolution() {
        assertEquals("TaskCh10N042Test.testInvolution", 32.0, involution(5, 2.0));
    }
}