package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

class TaskCh06N087 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter team #1: ");
        String teamName1 = input.next();
        System.out.print("Enter team #2: ");
        String teamName2 = input.next();
        Game game = new Game(teamName1, teamName2);
        game.play();
    }
}

class Game {
    private String teamName1;
    private String teamName2;
    private int scoreTeam1;
    private int scoreTeam2;

    public Game(String teamName1, String teamName2) {
        this.teamName1 = teamName1;
        this.teamName2 = teamName2;
    }

    public String getTeamName1() {
        return teamName1;
    }

    public String getTeamName2() {
        return teamName2;
    }

    public int getScoreTeam1() {
        return scoreTeam1;
    }

    public int getScoreTeam2() {
        return scoreTeam2;
    }

    public void setTeamName1(String tn1) {
        teamName1 = tn1;
    }

    public void setTeamName2(String tn2) {
        teamName2 = tn2;
    }

    public void setScoreTeam1(int st1) {
        scoreTeam1 = st1;
    }

    public void setScoreTeam2(int st2) {
        scoreTeam2 = st2;
    }

    public void play() {
        inputTeam1AndTeam2();
        int teamScore;
        int numberScore;
        do {
            teamScore = inputTeamScore();
            numberScore = inputScore(teamScore);
            if (teamScore == 1) {
                scoreTeam1 += numberScore;
            } else if (teamScore == 2) {
                scoreTeam2 += numberScore;
                result();
            }
        } while (teamScore != 0);
        System.out.print(result());
    }

    public void inputTeam1AndTeam2() {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter team #1:");
        teamName1 = input.nextLine();
        System.out.print("Enter team #2:");
        teamName2 = input.nextLine();
    }

    private int inputScore(int teamScore) {
        Scanner input = new Scanner(System.in);
        if (teamScore == 0) {
            return 0;
        }
        System.out.print("Enter score (1 or 2 or 3):");
        return input.nextInt();
    }

    private int inputTeamScore() {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter team to score (1 or 2 or 0 to finish game):");
        return input.nextInt();
    }

    public String result() {
        if (getScoreTeam1() == getScoreTeam2()) {
            return "deadheat " + getScore();
        } else {
            return getWinner() + " - winner, " + getLoser() + " - loser " + getScore();
        }
    }

    public String getScore() {
        return getScoreTeam1() + " : " + getScoreTeam2();
    }

    public String getWinner() {
        if (getScoreTeam1() > getScoreTeam2()) {
            return getTeamName1();
        }
        return getTeamName2();
    }

    public String getLoser() {
        return getTeamName1().equals(getWinner()) ? getTeamName2() : getTeamName1();
    }
}