package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh02N039.calcDegree;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

final class TaskCh02N039Test {
    public static void main(String[] args) {
        testCalcDegree();
    }

    private static void testCalcDegree() {
        assertEquals("TaskCh02N039Test.testCalcDegree", 90, calcDegree(3, 0, 0));
    }
}