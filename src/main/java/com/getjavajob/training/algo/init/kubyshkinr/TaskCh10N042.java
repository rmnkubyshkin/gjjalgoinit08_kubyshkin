package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.InputMismatchException;
import java.util.Scanner;

class TaskCh10N042 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(involution(input.nextInt(), input.nextDouble()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static double involution(int n, double number) {
        return (n == 0) ? 1 : number * involution(n - 1, number);
    }
}