package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh09N185.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

class TaskCh09N185Test {
    public static void main(String[] args) {
        testCheckNumberBrackets();
    }

    private static void testCheckNumberBrackets() {
        assertEquals("TaskCh09N166Test.testCheckNumberBrackets", "4", checkNumberBrackets("(D((SDF))FS(SDS)"));
        assertEquals("TaskCh09N166Test.testCheckNumberBrackets", "6", checkNumberBrackets("DSDssF))FS(SDS)"));
        assertEquals("TaskCh09N166Test.testCheckNumberBrackets", "YES", checkNumberBrackets("(D((SDF)FS))(SDS)"));
    }
}