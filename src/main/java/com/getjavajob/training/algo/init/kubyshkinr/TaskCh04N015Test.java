package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh04N015.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

final class TaskCh04N015Test {
    public static void main(String[] args) {
        testCalcAge();
    }

    private static void testCalcAge() {
        assertEquals("TaskCh04N015Test.testCalcAge", 29, calcAge(12, 2014, 6, 1985));
        assertEquals("TaskCh04N015Test.testCalcAge", 28, calcAge(5, 2014, 6, 1985));
        assertEquals("TaskCh04N015Test.testCalcAge", 29, calcAge(6, 2014, 6, 1985));
    }
}