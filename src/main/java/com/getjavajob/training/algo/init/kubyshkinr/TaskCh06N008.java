package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

import static java.lang.Math.*;

final class TaskCh06N008 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            printArray(getToSpecificNumberValues(input.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    private static void printArray(int[] array) {
        System.out.println(Arrays.toString(array));
    }

    public static int[] getToSpecificNumberValues(int n) {
        int[] array = new int[(int) sqrt(n)];
        for (int i = 0; i < array.length; i++) {
            int a = i + 1;
            array[i] = a * a;
        }
        return array;
    }
}