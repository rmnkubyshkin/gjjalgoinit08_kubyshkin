package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh10N041 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(fact(input.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static int fact(int n) {
        return n == 1 ? 1 : fact(n - 1) * n;
    }
}