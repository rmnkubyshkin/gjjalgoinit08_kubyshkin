package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh09N015 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(charAt(input.nextLine(), input.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static char charAt(String string, int k) {
        return string.charAt(k - 1);
    }
}