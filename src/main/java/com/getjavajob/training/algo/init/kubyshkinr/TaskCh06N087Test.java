package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N087Test {
    public static void main(String[] args) {
        testFinalDeadheatRes();
        testFinalWinnerRes();
    }

    private static void testFinalDeadheatRes() {
        Game game = new Game("team1", "team2");
        game.setScoreTeam1(6);
        game.setScoreTeam2(6);
        assertEquals("TaskCh06N087Test.testFinalDeadheatRes", "deadheat 6 : 6", game.result());
    }

    private static void testFinalWinnerRes() {
        Game game = new Game("team1", "team2");
        game.setScoreTeam1(2);
        game.setScoreTeam2(3);
        assertEquals("TaskCh06N087Test.testGetScore", "2 : 3", game.getScore());
        game.setScoreTeam1(5);
        game.setScoreTeam2(6);
        assertEquals("TaskCh06N087Test.testFinalWinnerRes", "team2 - winner, team1 - loser 5 : 6", game.result());
    }

}
