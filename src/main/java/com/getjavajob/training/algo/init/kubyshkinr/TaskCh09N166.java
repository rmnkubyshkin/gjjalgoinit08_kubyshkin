package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh09N166 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(swapFirstAndLastWord(input.nextLine()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static String swapFirstAndLastWord(String string) {
        String[] str = string.split(" ");
        String buf = str[0];
        str[0] = str[str.length - 1];
        str[str.length - 1] = buf;
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < str.length; i++) {
            result.append(str[i]);
            if (i != str.length - 1) {
                result.append(" ");
            }
        }
        return result.toString();
    }
}