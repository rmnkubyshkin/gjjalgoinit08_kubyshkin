package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

class TaskCh10N056 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.println(isPrime(input.nextInt(), 2));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static boolean isPrime(int number, int divider) {
        return number <= divider || number % divider != 0 && isPrime(number, divider + 1);
    }
}