package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TaskCh04N115 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(getAnimalAndColor(input.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    private static String getAnimal(int year) {
        switch (year % 12) {
            case 4:
                return "Rat";
            case 5:
                return "Cow";
            case 6:
                return "Tiger";
            case 7:
                return "Hare";
            case 8:
                return "Dragon";
            case 9:
                return "Snake";
            case 10:
                return "Horse";
            case 11:
                return "Sheep";
            case 12:
                return "Monkey";
            case 1:
                return "Cock";
            case 2:
                return "Dog";
            case 3:
                return "Pig";
            default:
                return null;
        }
    }

    private static String getColor(int year) {
        switch (year % 10) {
            case 1:
            case 0:
                return "Black";
            case 2:
            case 3:
                return "White";
            case 8:
            case 9:
                return "Yellow";
            case 4:
            case 5:
                return "Green";
            case 6:
            case 7:
                return "Red";
            default:
                return null;
        }
    }

    public static String getAnimalAndColor(int year) {
        return getColor(year) + " " + getAnimal(year);
    }
}