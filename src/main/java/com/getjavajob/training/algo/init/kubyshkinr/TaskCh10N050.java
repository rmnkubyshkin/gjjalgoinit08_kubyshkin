package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh10N050 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.println(calcFunctionAckermann(input.nextInt(), input.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static int calcFunctionAckermann(int n, int m) {
        if (n == 0) {
            return m + 1;
        } else if (m == 0) {
            return calcFunctionAckermann(n - 1, 1);
        } else {
            return calcFunctionAckermann(n - 1, calcFunctionAckermann(n, m - 1));
        }
    }
}