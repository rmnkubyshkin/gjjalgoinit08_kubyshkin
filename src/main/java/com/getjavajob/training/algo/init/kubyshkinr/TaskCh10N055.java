package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

class TaskCh10N055 {
    private static final char[] AFLETTERS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int number = input.nextInt();
            int radix = input.nextInt();
            System.out.println(transferToNumberSystem(number, radix));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static String transferToNumberSystem(int number, int radix) {
        if (number == 0) {
            return "";
        }
        return transferToNumberSystem(number / radix, radix) + AFLETTERS[number % radix];
    }
}