package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

class TaskCh10N048 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int n = input.nextInt();
            int[] array = getArray(n);
            System.out.println(findMaxElement(array.length - 1, array));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    private static int[] getArray(int n) {
        Scanner input = new Scanner(System.in);
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = input.nextInt();
        }
        return array;
    }

    public static int findMaxElement(int n, int[] array) {
        if (n == 0) {
            return array[0];
        }
        int max;
        int findMax = findMaxElement(n - 1, array);
        if (array[n] > findMax) {
            max = array[n];
        } else {
            return findMax;
        }
        return max;
    }
}