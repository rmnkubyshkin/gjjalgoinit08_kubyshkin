package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh12N024.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N024Test {
    public static void main(String[] args) {
        testCalcArray1();
        testCalcArray2();
    }

    private static void testCalcArray1() {
        int[][] expectArray = {
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252},
        };
        assertEquals("TaskCh12N024Test.testCalcArray1", expectArray, calcArray1());
    }

    private static void testCalcArray2() {
        int[][] expectArray = {
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5},
        };
        assertEquals("TaskCh12N024Test.testCalcArray2", expectArray, calcArray2());
    }
}