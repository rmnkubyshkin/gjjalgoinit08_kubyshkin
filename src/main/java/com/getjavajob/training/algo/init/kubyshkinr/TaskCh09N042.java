package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

class TaskCh09N042 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(invertStr(input.nextLine()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static String invertStr(String string) {
        return new StringBuilder(string).reverse().toString();
    }
}