package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh10N044.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N044Test {
    public static void main(String[] args) {
        testDigitalRoot();
    }

    private static void testDigitalRoot() {
        assertEquals("TaskCh10N044Test.testDigitalRoot", 6, getDigitalRoot(12345));
    }
}