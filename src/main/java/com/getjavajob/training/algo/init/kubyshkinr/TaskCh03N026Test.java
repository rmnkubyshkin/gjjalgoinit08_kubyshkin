package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh03N026.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

final class TaskCh03N026Test {
    public static void main(String[] args) {
        testCalculationFormula1();
        testCalculationFormula2();
        testCalculationFormula3();
    }

    private static void testCalculationFormula1() {
        assertEquals("TaskCh03N026Test.testCalculationFormula1 000", true, calculationFormula1(false, false, false));
        assertEquals("TaskCh03N026Test.testCalculationFormula1 001", true, calculationFormula1(false, false, true));
        assertEquals("TaskCh03N026Test.testCalculationFormula1 010", false, calculationFormula1(false, true, false));
        assertEquals("TaskCh03N026Test.testCalculationFormula1 011", false, calculationFormula1(false, true, true));
        assertEquals("TaskCh03N026Test.testCalculationFormula1 100", false, calculationFormula1(true, false, false));
        assertEquals("TaskCh03N026Test.testCalculationFormula1 101", false, calculationFormula1(true, false, true));
        assertEquals("TaskCh03N026Test.testCalculationFormula1 110", false, calculationFormula1(true, true, false));
        assertEquals("TaskCh03N026Test.testCalculationFormula1 111", false, calculationFormula1(true, true, true));
    }

    private static void testCalculationFormula2() {
        assertEquals("TaskCh03N026Test.testCalculationFormula2 000", true, calculationFormula2(false, false, false));
        assertEquals("TaskCh03N026Test.testCalculationFormula2 001", true, calculationFormula2(false, false, true));
        assertEquals("TaskCh03N026Test.testCalculationFormula2 010", false, calculationFormula2(false, true, false));
        assertEquals("TaskCh03N026Test.testCalculationFormula2 011", false, calculationFormula2(false, true, true));
        assertEquals("TaskCh03N026Test.testCalculationFormula2 100", true, calculationFormula2(true, false, false));
        assertEquals("TaskCh03N026Test.testCalculationFormula2 101", true, calculationFormula2(true, false, true));
        assertEquals("TaskCh03N026Test.testCalculationFormula2 110", true, calculationFormula2(true, true, false));
        assertEquals("TaskCh03N026Test.testCalculationFormula2 111", true, calculationFormula2(true, true, true));
    }

    private static void testCalculationFormula3() {
        assertEquals("TaskCh03N026Test.testCalculationFormula3 000", false, calculationFormula3(false, false, false));
        assertEquals("TaskCh03N026Test.testCalculationFormula3 001", true, calculationFormula3(false, false, true));
        assertEquals("TaskCh03N026Test.testCalculationFormula3 010", false, calculationFormula3(false, true, false));
        assertEquals("TaskCh03N026Test.testCalculationFormula3 011", false, calculationFormula3(false, true, true));
        assertEquals("TaskCh03N026Test.testCalculationFormula3 100", true, calculationFormula3(true, false, false));
        assertEquals("TaskCh03N026Test.testCalculationFormula3 101", true, calculationFormula3(true, false, true));
        assertEquals("TaskCh03N026Test.testCalculationFormula3 110", true, calculationFormula3(true, true, false));
        assertEquals("TaskCh03N026Test.testCalculationFormula3 111", true, calculationFormula3(true, true, true));
    }
}