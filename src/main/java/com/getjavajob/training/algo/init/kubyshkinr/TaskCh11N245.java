package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh11N245 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int n = input.nextInt();
            int[] array = getArray(n);
            printArray(sortArray(array));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    private static void printArray(int[] array) {
        System.out.println(Arrays.toString(array));
    }

    private static int[] getArray(int n) {
        Scanner input = new Scanner(System.in);
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = input.nextInt();
        }
        return array;
    }

    public static int[] sortArray(int[] initArray) {
        int[] resArray = new int[initArray.length];
        for (int i = 0,  k = resArray.length - 1, j = 0;  i < initArray.length; i++) {
            if (initArray[i] < 0) {
                resArray[j] = initArray[i];
                j++;
            } else {
                resArray[k] = initArray[i];
                k--;
            }
        }
        return resArray;
    }
}