package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh06N008.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N008Test {
    public static void main(String[] args) {
        testGetToSpecificNumberValues();
    }

    private static void testGetToSpecificNumberValues() {
        final int[] expectArray = {1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144};
        assertEquals("TaskCh06N008Test.testGetToSpecificNumberValues", expectArray, getToSpecificNumberValues(150));
    }
}