package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh04N036.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

final class TaskCh04N036Test {
    public static void main(String[] args) {
        testCalcColorOfTrafficLights();
    }

    private static void testCalcColorOfTrafficLights() {
        assertEquals("TaskCh04N036Test.testCalcColorOfTrafficLightsGreen", "green", calcColorOfTrafficLights(5));
        assertEquals("TaskCh04N036Test.calcColorOfTrafficLightsRed", "red", calcColorOfTrafficLights(3));
    }
}