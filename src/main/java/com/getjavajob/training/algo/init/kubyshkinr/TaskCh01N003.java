package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

final class TaskCh01N003 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.println("You entered number: " + input.nextInt());
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }
}