package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

class TaskCh10N043 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int n = input.nextInt();
            System.out.println(calcSumOfDigits(n));
            System.out.print(calcNumbersOfDigits(n));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static int calcSumOfDigits(int n) {
        if (n == 0) {
            return 0;
        }
        return n % 10 + calcSumOfDigits(n / 10);
    }

    public static int calcNumbersOfDigits(int n) {
        if (n == 0) {
            return 0;
        } else {
            return calcNumbersOfDigits(n / 10) + 1;
        }
    }
}