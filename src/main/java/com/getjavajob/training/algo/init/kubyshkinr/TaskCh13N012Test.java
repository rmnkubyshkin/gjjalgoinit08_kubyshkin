package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh13N012Test {
    public static void main(String[] args) {
        Database db = new Database();
        Employee client1 = new Employee("Ivan", "Ivanov", "Kantivirovskaya 8", 6, 2013, "Sergeevich");
        db.addToDatabase(client1);
        Employee client2 = new Employee("Igor", "Agapov", "Poteeva 10", 7, 2013);
        db.addToDatabase(client2);
        Employee client3 = new Employee("Andrey", "Semenov", "Soch 11", 5, 2013, "IVANovich");
        db.addToDatabase(client3);
        Employee client4 = new Employee("Anatoly", "Petrovskiy", "ABC 22", 1, 2005, "Andreevich");
        db.addToDatabase(client4);
        Employee client5 = new Employee("Robert", "Ivanovskiy", "Poplavay", 2, 2003, "Alekseevich");
        db.addToDatabase(client5);
        Employee client6 = new Employee("Michail", "Gatin", "Dudovskaya", 12, 2009, "Sechnivanova");
        db.addToDatabase(client6);
        Employee client7 = new Employee("Fedot", "Domin", "Frunze", 9, 2007, "Andreevich");
        db.addToDatabase(client7);
        Employee client8 = new Employee("Sergey", "Robertov", "Poplavay", 7, 2013, "IvAnovich");
        db.addToDatabase(client8);
        Employee client9 = new Employee("Ivan", "Kolosov", "Ssggg", 6, 2014, "Alekseevich");
        db.addToDatabase(client9);
        Employee client10 = new Employee("Dmitry", "Eremov", "Goosd", 8, 2007, "Romanovich");
        db.addToDatabase(client10);
        Employee client11 = new Employee("Fedya", "Supov", "Jojkina 8", 6, 2018, "Sergeevich");
        db.addToDatabase(client11);
        Employee client12 = new Employee("Igor", "Agapov", "ABCD 10", 7, 2014);
        db.addToDatabase(client12);
        Employee client13 = new Employee("Andrey", "Semenov", "Socheeva 11", 5, 2014, "Sergeevich");
        db.addToDatabase(client13);
        Employee client14 = new Employee("Anatoly", "Petrovskiy", "ABCreeva 12", 1, 2015, "Mihailovich");
        db.addToDatabase(client14);
        Employee client15 = new Employee("Robert", "Dudorov", "Govino 1", 9, 2014, "Alekseevich");
        db.addToDatabase(client15);
        Employee client16 = new Employee("Michail", "Gatin", "Samarskaya", 12, 2019, "Semenovich");
        db.addToDatabase(client16);
        Employee client17 = new Employee("Fedot", "Domin", "Ilyovskaya", 9, 2017, "Andreevich");
        db.addToDatabase(client17);
        Employee client18 = new Employee("Sergey", "Robertov", "Rubivo", 7, 2016, "Genad'evich");
        db.addToDatabase(client18);
        Employee client19 = new Employee("Dmitry", "Kolosov", "Toplevo", 6, 2015, "Ignat'evich");
        db.addToDatabase(client19);
        Employee client20 = new Employee("Dmitry", "Eremov", "Goosd", 8, 2014, "Romanovich");
        db.addToDatabase(client20);
        testGetWorkExperience(db);
        testFindSubstr(db);
    }

    private static void testGetWorkExperience(Database db) {
        List<Employee> expectedResult = new ArrayList<>(Arrays.asList(
                new Employee("Ivan", "Ivanov", "Kantivirovskaya 8", 6, 2013, "Sergeevich"),
                new Employee("Andrey", "Semenov", "Soch 11", 5, 2013, "IVANovich"),
                new Employee("Anatoly", "Petrovskiy", "ABC 22", 1, 2005, "Andreevich"),
                new Employee("Robert", "Ivanovskiy", "Poplavay", 2, 2003, "Alekseevich"),
                new Employee("Michail", "Gatin", "Dudovskaya", 12, 2009, "Sechnivanova"),
                new Employee("Fedot", "Domin", "Frunze", 9, 2007, "Andreevich"),
                new Employee("Dmitry", "Eremov", "Goosd", 8, 2007, "Romanovich")
        ));
        assertEquals("TaskCh13N012Test.testWorkExperience", expectedResult, db.getWorkExperience(6, 2016, 3));
    }

    private static void testFindSubstr(Database db) {
        List<Employee> expectedResult = new ArrayList<>(Arrays.asList(
                new Employee("Ivan", "Ivanov", "Kantivirovskaya 8", 6, 2013, "Sergeevich"),
                new Employee("Andrey", "Semenov", "Soch 11", 5, 2013, "IVANovich"),
                new Employee("Robert", "Ivanovskiy", "Poplavay", 2, 2003, "Alekseevich"),
                new Employee("Michail", "Gatin", "Dudovskaya", 12, 2009, "Sechnivanova"),
                new Employee("Sergey", "Robertov", "Poplavay", 7, 2013, "IvAnovich"),
                new Employee("Ivan", "Kolosov", "Ssggg", 6, 2014, "Alekseevich")
        ));
        assertEquals("TaskCh13N012Test.testFindSubstr", expectedResult, db.findSubstr("IvAn"));
    }
}
