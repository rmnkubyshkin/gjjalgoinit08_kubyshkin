package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

import static java.lang.Math.*;

final class TaskCh01N017 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.println(calculationFormula1(input.nextDouble()));
            System.out.println(calculationFormula2(input.nextDouble(), input.nextDouble(), input.nextDouble(), input.nextDouble()));
            System.out.println(calculationFormula3(input.nextDouble()));
            System.out.println(calculationFormula4(input.nextDouble()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    /**
     * o) Count value by a predetermined formula.
     */
    private static double calculationFormula1(double x) {
        return sqrt(1 - pow(sin(x), 2));
    }

    /**
     * p) Count value by a predetermined formula.
     */
    private static double calculationFormula2(double a, double b, double c, double x) {
        double quadFunc = a * pow(x, 2) + b * x + c;
        return 1 / sqrt(quadFunc);
    }

    /**
     * r) Count value by a predetermined formula.
     */
    private static double calculationFormula3(double x) {
        double numerator = sqrt(x + 1) + sqrt(x - 1);
        return numerator / (2 * sqrt(x));
    }

    /**
     * s) Count value by a predetermined formula.
     */
    private static double calculationFormula4(double x) {
        return abs(x) + abs(x + 1);
    }
}