package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh09N185 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(checkNumberBrackets(input.nextLine()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static String checkNumberBrackets(String arithmExp) {
        int counterOpenBrackets = 0;
        int counterClosedBrackets = 0;
        char[] sentence = arithmExp.toCharArray();
        for (int i = 0; i < arithmExp.length(); i++) {
            if (sentence[i] == '(') {
                counterOpenBrackets++;
            } else if (sentence[i] == ')') {
                counterClosedBrackets++;
            }
        }
        String result;
        if (counterOpenBrackets == counterClosedBrackets) {
            result = "YES";
        } else if (counterOpenBrackets < counterClosedBrackets) {
            result = Integer.toString(arithmExp.indexOf(')'));
        } else {
            result = Integer.toString(counterOpenBrackets);
        }
        return result;
    }
}