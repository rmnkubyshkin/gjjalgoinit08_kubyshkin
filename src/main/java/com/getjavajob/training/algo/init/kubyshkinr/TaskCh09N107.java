package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh09N107 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(swapFirstALastO(input.nextLine()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static String swapFirstALastO(String string) {
        int entryA = string.indexOf('a');
        int entryO = string.lastIndexOf('o');
        if (entryA != -1 && entryO != -1) {
            char[] charArr = string.toCharArray();
            charArr[entryA] = 'o';
            charArr[entryO] = 'a';
            string = String.valueOf(charArr);
        } else {
            string = "There are no letters a,o";
        }
        return string;
    }
}