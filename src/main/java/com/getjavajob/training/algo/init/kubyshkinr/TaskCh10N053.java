package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

class TaskCh10N053 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int n = input.nextInt();
            int[] array = getArray(n);
            printArray(invertNumbers(n, array));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    private static void printArray(int[] array) {
        System.out.println(Arrays.toString(array));
    }

    private static int[] getArray(int n) {
        Scanner input = new Scanner(System.in);
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = input.nextInt();
        }
        return array;
    }

    public static int[] invertNumbers(int n, int[] array) {
        if (n == 0) {
            return array;
        }
        if (n > array.length / 2) {
            int a = array[n - 1];
            array[n - 1] = array[array.length - n];
            array[array.length - n] = a;
        }
        return invertNumbers(n - 1, array);
    }
}