package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh02N043.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

final class TaskCh02N043Test {
    public static void main(String[] args) {
        testCalcDividedArgs();
    }

    private static void testCalcDividedArgs() {
        assertEquals("TaskCh02N043Test.testCalcDividedArgs", 1, calcDividedArgs(15.0, 3.0));
    }
}