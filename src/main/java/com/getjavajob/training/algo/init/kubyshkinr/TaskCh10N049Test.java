package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh10N049.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N049Test {
    public static void main(String[] args) {
        testFindMaxIndex();
    }

    private static void testFindMaxIndex() {
        int[] array = new int[]{1022, 200, 4, 3, 1201};
        assertEquals("TaskCh10N049Test.testFindMaxIndex", 4, findIndexMaxElement(findMaxElement(4, array), array));
    }
}
