package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh05N038.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

final class TaskCh05N038Test {
    public static void main(String[] args) {
        testCalcAlternatingHarmonicSeries();
        testCalcHarmonicSeries();
    }

    private static void testCalcAlternatingHarmonicSeries() {
        assertEquals("TaskCh05N038Test.testCalcAlternatingHarmonicSeries", 0.5, calcAlternatingHarmonicSeries(2));
    }

    private static void testCalcHarmonicSeries() {
        assertEquals("TaskCh05N038Test.testCalcHarmonicSeries", 1.5, calcHarmonicSeries(2));
    }
}