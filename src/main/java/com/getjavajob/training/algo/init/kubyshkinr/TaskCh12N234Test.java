package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh12N234.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N234Test {
    public static void main(String[] args) {
        testDeleteLine();
        testDeleteColumn();
    }

    private static void testDeleteLine() {
        int[][] expectArray = {{5, 5, 5, 5, 5},
                {1, 2, 3, 4, 5},
                {4, 4, 4, 4, 4},
                {5, 6, 7, 8, 9},
                {0, 0, 0, 0, 0}};
        int[][] array = {{5, 5, 5, 5, 5},
                {1, 2, 3, 4, 5},
                {4, 4, 4, 4, 4},
                {7, 7, 7, 7, 7},
                {5, 6, 7, 8, 9}};
        deleteLine(array, 3);
        assertEquals("TaskCh12N234Test.testDeleteLine", expectArray, array);
    }

    private static void testDeleteColumn() {
        int[][] expectArray = {{1, 5, 8, 7, 0},
                {1, 5, 8, 7, 0},
                {1, 5, 8, 7, 0},
                {1, 5, 8, 7, 0},
                {1, 5, 8, 7, 0}};
        int[][] array = {{1, 2, 5, 8, 7},
                {1, 2, 5, 8, 7},
                {1, 2, 5, 8, 7},
                {1, 2, 5, 8, 7},
                {1, 2, 5, 8, 7}};
        deleteColumn(array, 1);
        assertEquals("TaskCh12N234Test.testDeleteColumn", expectArray, array);
    }
}