package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh05N010.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

final class TaskCh05N010Test {
    public static void main(String[] args) {
        testCalcDollars();
    }

    private static void testCalcDollars() {
        double[] expectArr = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0};
        assertEquals("TaskCh05N010Test.testCalcDollars", expectArr, calcDollars(1));
    }
}