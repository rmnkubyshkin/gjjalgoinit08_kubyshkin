package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh10N045.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N045Test {
    public static void main(String[] args) {
        testGetElementOfArithmeticProgression();
        testCalcSumOfArithmeticProgression();
    }

    private static void testGetElementOfArithmeticProgression() {
        assertEquals("TaskCh10N045Test.testGetElementOfArithmeticProgression", 33, getElementOfArithmeticProgression(6, 5, 3));
    }

    private static void testCalcSumOfArithmeticProgression() {
        assertEquals("TaskCh10N045Test.testCalcSumOfArithmeticProgression", 126, calcSumOfArithmeticProgression(6, 5, 3));
    }
}