package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

final class TaskCh03N029 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.println(formulaA(input.nextInt(), input.nextInt()));
            System.out.println(formulaB(input.nextInt(), input.nextInt()));
            System.out.println(formulaC(input.nextInt(), input.nextInt()));
            System.out.println(formulaD(input.nextInt(), input.nextInt(), input.nextInt()));
            System.out.println(formulaE(input.nextInt(), input.nextInt(), input.nextInt()));
            System.out.println(formulaF(input.nextInt(), input.nextInt(), input.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    /**
     * a) Each of the numbers X and Y is odd.
     */
    public static boolean formulaA(int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    /**
     * b) Only one of the numbers X and Y is less than or equal to 20.
     */
    public static boolean formulaB(int x, int y) {
        return x < 20 ^ y < 20;
    }

    /**
     * c) At least one of the numbers X and Y equals zero.
     */
    public static boolean formulaC(int x, int y) {
        return x == 0 || y == 0;
    }

    /**
     * d) Each of the numbers X, Y, Z is negative.
     */
    public static boolean formulaD(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    /**
     * e) Only one of the numbers X, Y and Z is a multiple of five.
     */
    public static boolean formulaE(int x, int y, int z) {
        boolean a = x % 5 == 0;
        boolean b = y % 5 == 0;
        boolean c = z % 5 == 0;
        return a ^ b ^ c && !(a && b && c);
    }

    /**
     * f) At least one of the numbers X, Y, Z is more than 100.
     */
    public static boolean formulaF(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}