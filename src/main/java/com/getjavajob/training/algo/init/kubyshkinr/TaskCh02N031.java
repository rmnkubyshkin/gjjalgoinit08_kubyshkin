package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

final class TaskCh02N031 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(swapLastTwoDigits(input.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static int swapLastTwoDigits(int n) {
        int firstValue = n / 100 * 100;
        int secondValue = n % 10 * 10;
        int thirdValue = n / 10 % 10;
        return firstValue + secondValue + thirdValue;
    }
}