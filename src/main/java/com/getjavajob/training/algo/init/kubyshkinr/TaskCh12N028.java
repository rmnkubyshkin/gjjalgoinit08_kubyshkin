package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.Arrays;

public class TaskCh12N028 {
    private static final int SIZE = 5;

    public static void main(String[] args) {
        printArray(calcArray());
    }

    private static void printArray(int[][] array) {
        System.out.println(Arrays.deepToString(array));
    }

    public static int[][] calcArray() {
        int[][] array = new int[SIZE][SIZE];
        int x = 0;
        int y = 1;
        for (int i = 0; x < SIZE * SIZE; i++) {
            for (int j = y - 1; j < array[0].length - (y - 1); j++) {
                array[i][j] = x + 1;
                x++;
            }
            i = array[0].length - y;
            for (int j = y; j < array.length - (y - 1); j++) {
                array[j][i] = x + 1;
                x++;
            }
            for (int j = array[0].length - 1 - y; j >= y - 1; j--) {
                array[i][j] = x + 1;
                x++;
            }
            i = y - 1;
            for (int j = array.length - 1 - y; j >= y; j--) {
                array[j][i] = x + 1;
                x++;
            }
            y++;
        }
        return array;
    }
}