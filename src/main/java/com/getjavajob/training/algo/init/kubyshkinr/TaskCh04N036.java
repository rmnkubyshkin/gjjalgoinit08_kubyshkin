package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

final class TaskCh04N036 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(calcColorOfTrafficLights(input.nextDouble()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static String calcColorOfTrafficLights(double t) {
        return t % 5 < 3 ? "green" : "red";
    }
}