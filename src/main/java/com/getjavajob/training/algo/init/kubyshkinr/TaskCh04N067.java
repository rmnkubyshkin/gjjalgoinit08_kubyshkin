package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

final class TaskCh04N067 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(calcDayOfWeek(input.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static String calcDayOfWeek(int k) {
        int bufK = k;
        bufK %= 7;
        return bufK >= 1 && bufK <= 5 ? "Workday" : "Weekend";
    }
}

