package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TaskCh09N017 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(isBeginsAndEndsWithSameLetter(input.nextLine()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static boolean isBeginsAndEndsWithSameLetter(String string) {
        return string.charAt(0) == string.charAt(string.length() - 1);
    }
}