package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh10N047.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N047Test {
    public static void main(String[] args) {
        testGetElementFibonacci();
    }

    private static void testGetElementFibonacci() {
        assertEquals("TaskCh10N047Test.testGetElementFibonacci", 55, getElementFibonacci(10));
    }
}
