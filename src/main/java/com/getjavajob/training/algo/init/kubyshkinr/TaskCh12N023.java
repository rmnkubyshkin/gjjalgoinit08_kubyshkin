package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.Arrays;

public class TaskCh12N023 {
    private static final int SIZE = 7;

    public static void main(String[] args) {
        printArray(calcArray1());
        printArray(calcArray2());
        printArray(calcArray3());
    }

    private static void printArray(int[][] array) {
        System.out.println(Arrays.deepToString(array));
    }

    public static int[][] calcArray1() {
        int[][] array = new int[SIZE][SIZE];
        for (int i = 0, k = 0; i < array.length; i++, k++) {
            array[i][k] = 1;
            array[i][array.length - 1 - k] = 1;
        }
        return array;
    }

    public static int[][] calcArray2() {
        int[][] array = new int[SIZE][SIZE];
        for (int i = 0, k = 0; i < array.length; i++, k++) {
            array[i][k] = 1;
            array[i][array.length - 1 - k] = 1;
            array[i][array.length / 2] = 1;
            array[array.length / 2][i] = 1;
        }
        return array;
    }

    public static int[][] calcArray3() {
        int[][] array = new int[SIZE][SIZE];
        int count = 0;
        for (int i = 0; i < array.length / 2 + 1; i++) {
            for (int k = 0; k < array.length / 2 + 1; k++) {
                if (k + count <= array.length - 1 - k - count) {
                    array[i][k + count] = 1;
                    array[i][array.length - 1 - k - count] = 1;
                }
            }
            count++;
        }
        count = 0;
        for (int i = array.length - 1; i > array.length / 2; i--) {
            for (int k = 0; k < array.length / 2 + 1; k++) {
                if (k + count <= array.length - 1 - k - count) {
                    array[i][k + count] = 1;
                    array[i][array.length - 1 - k - count] = 1;
                }
            }
            count++;
        }
        return array;
    }
}