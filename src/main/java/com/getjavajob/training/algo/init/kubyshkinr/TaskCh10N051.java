package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh10N051 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int n = input.nextInt();
            method1(n);
            System.out.println();
            method2(n);
            System.out.println();
            method3(n);
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    private static void method1(int n) {
        if (n > 0) {
            System.out.println(n);
            method1(n - 1);
        }
    }

    private static void method2(int n) {
        if (n > 0) {
            method2(n - 1);
            System.out.println(n);
        }
    }

    private static void method3(int n) {
        if (n > 0) {
            System.out.println(n);
            method3(n - 1);
        }
        System.out.println(n);
    }
}