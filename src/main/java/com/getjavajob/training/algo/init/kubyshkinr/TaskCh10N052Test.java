package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh10N052.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N052Test {
    public static void main(String[] args) {
        testInvertDigits();
    }

    private static void testInvertDigits() {
        assertEquals("TaskCh10N052Test.testInvertDigits", 54321, invertDigits(12345));
    }
}
