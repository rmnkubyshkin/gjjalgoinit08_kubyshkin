package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh10N046.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N046Test {
    public static void main(String[] args) {
        testGetElementOfGeometricProgression();
        testCalcSumOfGeometricProgression();
    }

    private static void testGetElementOfGeometricProgression() {
        assertEquals("TaskCh10N046Test.testGetElementOfGeometricProgression", 27, getElementOfGeometricProgression(3, 3, 1));
    }

    private static void testCalcSumOfGeometricProgression() {
        assertEquals("TaskCh10N046Test.testCalcSumOfGeometricProgression", 40, calcSumOfGeometricProgression(3, 3, 1));
    }
}