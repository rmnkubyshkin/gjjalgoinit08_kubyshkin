package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

final class TaskCh02N043 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.println(calcDividedArgs(input.nextDouble(), input.nextDouble()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static double calcDividedArgs(double a, double b) {
        double div = 0;
        if (a != 0 && b != 0) {
            div = a % b * (b % a) + 1;
        } else {
            System.out.println("Incorrect input");
        }
        return div;
    }
}