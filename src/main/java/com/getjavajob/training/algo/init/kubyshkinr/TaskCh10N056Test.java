package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh10N056.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N056Test {
    public static void main(String[] args) {
        testIsPrime();
    }

    private static void testIsPrime() {
        assertEquals("TaskCh10N053Test.testIsPrime", true, isPrime(17, 2));
        assertEquals("TaskCh10N053Test.testIsPrime", false, isPrime(14, 2));
    }
}