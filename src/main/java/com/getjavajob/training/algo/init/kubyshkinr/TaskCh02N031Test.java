package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh02N031.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

final class TaskCh02N031Test {
    public static void main(String[] args) {
        testSwapLastTwoDigits();
    }

    private static void testSwapLastTwoDigits() {
        assertEquals("TaskCh02N031Test.testSwapLastTwoDigits", 305, swapLastTwoDigits(350));
    }
}