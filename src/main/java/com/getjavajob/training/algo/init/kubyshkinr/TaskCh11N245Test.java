package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh11N245.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N245Test {
    public static void main(String[] args) {
        testSortArray();
    }

    private static void testSortArray() {
        int[] expectArray = {-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5};
        int[] array = {5, 4, 3, 2, 1, 0, -5, -4, -3, -2, -1};
        assertEquals("TaskCh11N245Test.testSortArray", expectArray, sortArray(array));
    }
}
