package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh13N012 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            Database db = new Database();
            Employee client1 = new Employee("Ivan", "Ivanov", "Kantivirovskaya 8", 6, 2013, "Sergeevich");
            db.addToDatabase(client1);
            Employee client2 = new Employee("Igor", "Agapov", "Poteeva 10", 7, 2013);
            db.addToDatabase(client2);
            Employee client3 = new Employee("Andrey", "Semenov", "Soch 11", 5, 2013, "IVANovich");
            db.addToDatabase(client3);
            Employee client4 = new Employee("Anatoly", "Petrovskiy", "ABC 22", 1, 2005, "Andreevich");
            db.addToDatabase(client4);
            Employee client5 = new Employee("Robert", "Ivanovskiy", "Poplavay", 2, 2003, "Alekseevich");
            db.addToDatabase(client5);
            Employee client6 = new Employee("Michail", "Gatin", "Dudovskaya", 12, 2009, "Sechnivanova");
            db.addToDatabase(client6);
            Employee client7 = new Employee("Fedot", "Domin", "Frunze", 9, 2007, "Andreevich");
            db.addToDatabase(client7);
            Employee client8 = new Employee("Sergey", "Robertov", "Poplavay", 7, 2013, "IvAnovich");
            db.addToDatabase(client8);
            Employee client9 = new Employee("Ivan", "Kolosov", "Ssggg", 6, 2014, "Alekseevich");
            db.addToDatabase(client9);
            Employee client10 = new Employee("Dmitry", "Eremov", "Goosd", 8, 2007, "Romanovich");
            db.addToDatabase(client10);
            Employee client11 = new Employee("Fedya", "Supov", "Jojkina 8", 6, 2018, "Sergeevich");
            db.addToDatabase(client11);
            Employee client12 = new Employee("Igor", "Agapov", "ABCD 10", 7, 2014);
            db.addToDatabase(client12);
            Employee client13 = new Employee("Andrey", "Semenov", "Socheeva 11", 5, 2014, "Sergeevich");
            db.addToDatabase(client13);
            Employee client14 = new Employee("Anatoly", "Petrovskiy", "ABCreeva 12", 1, 2015, "Mihailovich");
            db.addToDatabase(client14);
            Employee client15 = new Employee("Robert", "Dudorov", "Govino 1", 9, 2014, "Alekseevich");
            db.addToDatabase(client15);
            Employee client16 = new Employee("Michail", "Gatin", "Samarskaya", 12, 2019, "Semenovich");
            db.addToDatabase(client16);
            Employee client17 = new Employee("Fedot", "Domin", "Ilyovskaya", 9, 2017, "Andreevich");
            db.addToDatabase(client17);
            Employee client18 = new Employee("Sergey", "Robertov", "Rubivo", 7, 2016, "Genad'evich");
            db.addToDatabase(client18);
            Employee client19 = new Employee("Dmitry", "Kolosov", "Toplevo", 6, 2015, "Ignat'evich");
            db.addToDatabase(client19);
            Employee client20 = new Employee("Dmitry", "Eremov", "Goosd", 8, 2014, "Romanovich");
            db.addToDatabase(client20);
            System.out.println("enter actualMonth, actualYear and exp ");
            db.getWorkExperience(input.nextInt(), input.nextInt(), input.nextInt());
            System.out.println("enter substring");
            db.findSubstr(input.next());
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }
}

class Employee {
    private String firstName;
    private String lastName;
    private String patronymic;
    private String address;
    private int employmentMonth;
    private int employmentYear;

    public Employee(String firstName, String lastName, String address, int employmentMonth, int employmentYear) {
        setFirstName(firstName);
        setLastName(lastName);
        setAddress(address);
        setMonth(employmentMonth);
        setYear(employmentYear);
    }

    public Employee(String firstName, String lastName, String address, int month, int year, String patronymic) {
        this(firstName, lastName, address, month, year);
        setPatronymic(patronymic);
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setMonth(int month) {
        this.employmentMonth = month;
    }

    public void setYear(int year) {
        this.employmentYear = year;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getAddress() {
        return address;
    }

    public int getEmploymentMonth() {
        return employmentMonth;
    }

    public int getEmploymentYear() {
        return employmentYear;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        Employee other = (Employee) obj;
        if (getEmploymentMonth() != other.getEmploymentMonth()) {
            return false;
        } else if (getPatronymic() != other.getPatronymic()) {
            return false;
        } else if (getLastName() != other.getLastName()) {
            return false;
        } else if (getAddress() != other.getAddress()) {
            return false;
        } else if (getFirstName() != other.getFirstName()) {
            return false;
        } else if (getEmploymentYear() != other.getEmploymentYear()) {
            return false;
        }
        return true;
    }

    public int getWorkYears(int actualMonth, int actualYear) {
        int difference = actualYear - employmentYear;
        if (actualMonth < employmentMonth) {
            difference -= 1;
        }
        return difference;
    }
}

class Database {
    private List<Employee> employeeDatabase;

    public Database() {
        employeeDatabase = new ArrayList<>();
    }

    public List<Employee> getEmployeeDatabase() {
        return employeeDatabase;
    }

    public void addToDatabase(Employee employee) {
        employeeDatabase.add(employee);
    }

    public List<Employee> getWorkExperience(int actualMonth, int actualYear, int exp) {
        List<Employee> resultEmployee = new ArrayList<>();
        List<Employee> employee = getEmployeeDatabase();
        for (Employee getEmployee : employee) {
            if (getEmployee.getWorkYears(actualMonth, actualYear) >= exp) {
                resultEmployee.add(getEmployee);
            }
        }
        return resultEmployee;
    }

    public List<Employee> findSubstr(String substring) {
        List<Employee> resultEmployee = new ArrayList<>();
        List<Employee> employee = getEmployeeDatabase();
        String str2 = substring.toLowerCase();
        for (Employee getEmployee : employee) {
            boolean empFirstName = getEmployee.getFirstName().toLowerCase().contains(str2);
            boolean empLastName = getEmployee.getLastName().toLowerCase().contains(str2);
            boolean empMidName;
            if (getEmployee.getPatronymic() != null) {
                empMidName = getEmployee.getPatronymic().toLowerCase().contains(str2);
            } else {
                empMidName = false;
            }
            if (empFirstName || empLastName || empMidName) {
                resultEmployee.add(getEmployee);
            }
        }
        return resultEmployee;
    }
}