package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

class TaskCh10N045 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int firstElement = input.nextInt();
            int difference = input.nextInt();
            int n = input.nextInt();
            System.out.println(getElementOfArithmeticProgression(n, difference, firstElement));
            System.out.println(calcSumOfArithmeticProgression(n, difference, firstElement));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static int getElementOfArithmeticProgression(int n, int d, int a1) {
        return n == 0 ? a1 : getElementOfArithmeticProgression(n - 1, d, a1) + d;
    }

    public static int calcSumOfArithmeticProgression(int n, int d, int a1) {
        return n == 0 ? a1 : calcSumOfArithmeticProgression(n - 1, d, a1) + getElementOfArithmeticProgression(n, d, a1);
    }
}