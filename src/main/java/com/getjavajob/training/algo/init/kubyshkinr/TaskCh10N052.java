package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

class TaskCh10N052 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.println(invertDigits(input.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static int invertDigits(int n) {
        if (n < 10) {
            return n;
        }
        int multiplier = 10;
        while (multiplier < n) {
            multiplier *= 10;
        }
        multiplier /= 10;
        return n % 10 * multiplier + invertDigits(n / 10);
    }
}
