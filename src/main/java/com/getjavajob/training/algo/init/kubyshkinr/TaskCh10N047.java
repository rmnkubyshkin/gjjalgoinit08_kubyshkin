package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh10N047 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.println(getElementFibonacci(input.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static int getElementFibonacci(int n) {
        return n < 2 ? n : getElementFibonacci(n - 1) + getElementFibonacci(n - 2);
    }
}