package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh12N024 {
    private static final int SIZE = 6;

    public static void main(String[] args) {
        printArray(calcArray1());
        printArray(calcArray2());
    }

    private static void printArray(int[][] array) {
        System.out.println(Arrays.deepToString(array));
    }

    public static int[][] calcArray1() {
        int[][] array = new int[SIZE][SIZE];
        for (int i = 0; i < array.length; i++) {
            array[0][i] = 1;
            array[i][0] = 1;
        }
        for (int i = 1; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                array[i][j + 1] = array[i - 1][j + 1] + array[i][j];
            }
        }
        return array;
    }

    public static int[][] calcArray2() {
        int[][] array = new int[SIZE][SIZE];
        for (int i = 0; i < array.length; i++) {
            array[0][i] = i + 1;
        }
        for (int i = 1; i < array.length; i++) {
            for (int j = 0; j <= array.length - 1; j++) {
                array[i][j] = (j != array.length - 1) ? array[i - 1][j + 1] : array[i - 1][0];
            }
        }
        return array;
    }
}