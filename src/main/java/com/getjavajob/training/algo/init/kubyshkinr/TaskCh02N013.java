package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

final class TaskCh02N013 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(calcInvert(input.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static int calcInvert(int value) {
        int result = 0;
        int valueBuf = value;
        while (valueBuf > 0) {
            result = result * 10 + valueBuf % 10;
            valueBuf /= 10;
        }
        return result;
    }
}