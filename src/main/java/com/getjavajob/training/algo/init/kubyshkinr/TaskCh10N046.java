package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

class TaskCh10N046 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int firstElement = input.nextInt();
            int difference = input.nextInt();
            int n = input.nextInt();
            System.out.println(getElementOfGeometricProgression(n, difference, firstElement));
            System.out.println(calcSumOfGeometricProgression(n, difference, firstElement));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static int getElementOfGeometricProgression(int n, int d, int a1) {
        return n == 0 ? a1 : getElementOfGeometricProgression(n - 1, d, a1) * d;
    }

    public static int calcSumOfGeometricProgression(int n, int d, int a1) {
        return n == 0 ? a1 : calcSumOfGeometricProgression(n - 1, d, a1) + getElementOfGeometricProgression(n, d, a1);
    }
}