package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh10N055.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N055Test {
    public static void main(String[] args) {
        testTransferToNumberSystem();
    }

    private static void testTransferToNumberSystem() {
        assertEquals("TaskCh10N053Test.testGetResBinaryNS", "10001011", transferToNumberSystem(139, 2));
        assertEquals("TaskCh10N053Test.testGetResTernaryNS", "12011", transferToNumberSystem(139, 3));
        assertEquals("TaskCh10N053Test.testGetResFourfoldNS", "2023", transferToNumberSystem(139, 4));
        assertEquals("TaskCh10N053Test.testGetResFivefoldNS", "1024", transferToNumberSystem(139, 5));
        assertEquals("TaskCh10N053Test.testGetResSextupleNS", "351", transferToNumberSystem(139, 6));
        assertEquals("TaskCh10N053Test.testGetResSevenfoldNS", "256", transferToNumberSystem(139, 7));
        assertEquals("TaskCh10N053Test.testGetResOctalNS", "213", transferToNumberSystem(139, 8));
        assertEquals("TaskCh10N053Test.testGetResNonaryNS", "164", transferToNumberSystem(139, 9));
        assertEquals("TaskCh10N053Test.testGetResElevenfoldNS", "117", transferToNumberSystem(139, 11));
        assertEquals("TaskCh10N053Test.testGetResDuodecimalNS", "b7", transferToNumberSystem(139, 12));
        assertEquals("TaskCh10N053Test.testGetResThirteenfoldNS", "a9", transferToNumberSystem(139, 13));
        assertEquals("TaskCh10N053Test.testGetResFourteenfoldNS", "9d", transferToNumberSystem(139, 14));
        assertEquals("TaskCh10N053Test.testGetResFifteenfoldNS", "94", transferToNumberSystem(139, 15));
        assertEquals("TaskCh10N053Test.testGetResHexadecimalNS", "8b", transferToNumberSystem(139, 16));
    }
}