package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh10N044 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(getDigitalRoot(input.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static int getDigitalRoot(int n) {
        if (n < 10) {
            return n;
        } else {
            return getDigitalRoot(digitalRoot(n));
        }
    }

    private static int digitalRoot(int n) {
        if (n == 0) {
            return 0;
        }
        return n % 10 + digitalRoot(n / 10);
    }
}
