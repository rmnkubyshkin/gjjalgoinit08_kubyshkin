package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh11N158.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N158Test {
    public static void main(String[] args) {
        testRemoveSameNumbers();
    }

    private static void testRemoveSameNumbers() {
        int[] expectArray = {5, 4, 3, 2, 1, 0, 0};
        int[] array = new int[]{5, 5, 4, 3, 2, 2, 1};
        assertEquals("TaskCh11N158Test.removeSameNumbers", expectArray, removeSameNumbers(array));
    }
}