package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

public class TaskCh05N038 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int n = input.nextInt();
            System.out.println(calcAlternatingHarmonicSeries(n));
            System.out.println(calcHarmonicSeries(n));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static double calcAlternatingHarmonicSeries(int n) {
        double distance = 0;
        for (int i = 1; i <= n; ++i) {
            distance += (2 * (i % 2) - 1) / (i + 0.);
        }
        return distance;
    }

    public static double calcHarmonicSeries(int n) {
        double way = 0;
        for (int i = 1; i <= n; ++i) {
            way += 1 / (i + 0.);
        }
        return way;
    }
}