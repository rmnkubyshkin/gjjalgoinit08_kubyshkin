package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh04N115.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

final class TaskCh04N115Test {
    public static void main(String[] args) {
        testGetAnimalAndColor();
    }

    private static void testGetAnimalAndColor() {
        assertEquals("TaskCh04N115Test.testGetAnimalAndColor", "Yellow Cow", getAnimalAndColor(2009));
    }
}