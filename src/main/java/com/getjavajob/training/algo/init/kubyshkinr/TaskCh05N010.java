package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

class TaskCh05N010 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(Arrays.toString(calcDollars(input.nextDouble())));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static double[] calcDollars(double rate) {
        double[] money = new double[20];
        for (int i = 0; i < money.length; i++) {
            money[i] = (i + 1) * rate;
        }
        return money;
    }
}