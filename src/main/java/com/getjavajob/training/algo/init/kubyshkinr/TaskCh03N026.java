package com.getjavajob.training.algo.init.kubyshkinr;

import java.util.*;

final class TaskCh03N026 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print(calculationFormula1(input.nextBoolean(), input.nextBoolean(), input.nextBoolean()));
            System.out.print(calculationFormula2(input.nextBoolean(), input.nextBoolean(), input.nextBoolean()));
            System.out.print(calculationFormula3(input.nextBoolean(), input.nextBoolean(), input.nextBoolean()));
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input");
        }
    }

    public static boolean calculationFormula1(boolean x, boolean y, boolean z) {
        return !x && !y && (!x || !z);
    }

    public static boolean calculationFormula2(boolean x, boolean y, boolean z) {
        return x || !y || (x && !z);
    }

    public static boolean calculationFormula3(boolean x, boolean y, boolean z) {
        return x || !y && !x && z;
    }
}