package com.getjavajob.training.algo.init.kubyshkinr;

import static com.getjavajob.training.algo.init.kubyshkinr.TaskCh10N050.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N050Test {
    public static void main(String[] args) {
        testCalcFunctionAckermann();
    }

    private static void testCalcFunctionAckermann() {
        assertEquals("TaskCh10N050Test.testCalcFunctionAckermann", 5, calcFunctionAckermann(1, 3));
    }
}
